import com.ktorcommittee.ChatSession
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.Routing
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.consumeEach

suspend fun handleMessage(id: String, message: String, chatService: ChatService) {
    when {
        else -> chatService.broadcast(id, message)
    }
}

fun Routing.chat() {
    val chatService = ChatService()

    webSocket("/chat") {

        val session = call.sessions.get<ChatSession>()

        if (session == null) {
            close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "There is no session available."))
            return@webSocket
        }

        chatService.join(session.id, this)

        try {
            incoming.consumeEach { frame ->
                if (frame is Frame.Text) {
                    handleMessage(session.id, frame.readText(), chatService)
                }
            }
        } finally {
            chatService.leave(session.id, this)
        }
    }
}