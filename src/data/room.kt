//Use as entity with DB for save messages
//data class Message(
////  val messageType: SenderType,
////  val message: String,
////  val sessionId: String,
////  val messageOwner: String
////)

//simle data for room this should be an entity too for db
//data class Room(val id: String, val name: String)

//Use to clasified the message to sender
enum class SenderType(val type: String) {
  Self("self"),
  System("server")
}