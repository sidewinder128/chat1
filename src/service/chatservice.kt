import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicInteger

class ChatService {

    val usersCount = AtomicInteger()

    val userNames = ConcurrentHashMap<String, String>()

    val users = ConcurrentHashMap<String, MutableList<WebSocketSession>>()

    //This should go to DB, just for now in memory
    val messageHistory = CopyOnWriteArrayList<String>()

    suspend fun join(user: String, socket: WebSocketSession) {

        //There is a better way t handle this to check if the user already joined, could be used DB
        val newUserList = users.computeIfAbsent(user) { CopyOnWriteArrayList<WebSocketSession>() }
        //also add the current socket, if there is 2 items in the list means already joined user, if not means new user joined
        newUserList.add(socket)

        //If the result of the new user list is just one user, so it will test it as a new user and then broadcast message
        if (newUserList.size == 1) {
            val userName = userNames.computeIfAbsent(user) { "user${usersCount.incrementAndGet()}" }
            broadcast(SenderType.System.type, "User joined: $userName.")
        }

        //simulate getting all message history from db to new user, but for now just memory
        for (message in messageHistory) {
            socket.send(Frame.Text(message))
        }
    }

    suspend fun leave(user: String, socket: WebSocketSession) {

        val userSocketSessions = users[user]
        userSocketSessions?.remove(socket)
        userSocketSessions?.let {
            if (userSocketSessions.isEmpty()) {
                val userName = userNames.remove(user) ?: user
                broadcast(SenderType.System.type, "User left: $userName.")
            }
        }
    }

    //We need to add some logic here to handle the room when broadcast to users
    suspend fun broadcast(sender: String, message: String) {
        val userName = userNames[sender] ?: sender
        users.values.forEach { socket ->
            val formattedMessage="[$userName] $message"
            socket.send(Frame.Text(formattedMessage))

            //save to db the message but for now just to memory
            messageHistory.add(formattedMessage)
            if (messageHistory.size > 100) {
                messageHistory.removeAt(0)
            }
        }
    }

    //Extension method to send to all websockets connected on server
    //This extension captures all websoket sessions that are active
    //We need to divide messages by room in here
    suspend fun List<WebSocketSession>.send(frame: Frame) {
        forEach {
            it.send(frame.copy())
        }
    }
}